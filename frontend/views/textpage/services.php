<?php
if (Yii::$app->language == 'en') {
    $seoTitle = $model->en_seo_title;
    $seoDescription = $model->en_seo_description;
    $seoKeywords = $model->en_seo_keywords;
    $seoH1 = $model->en_seo_h1;

    $name = $model->en_name;
} else {
    $seoTitle = $model->seo_title;
    $seoDescription = $model->seo_description;
    $seoKeywords = $model->seo_keywords;
    $seoH1 = $model->seo_h1;

    $name = $model->name;
}
?>
<div class="mainSection company">
    <div class="container">
        <div class="header"><span><?=(empty($seoH1)) ? $name : $seoH1 ?></span></div>
        <div class="light text-center" style="margin: 20px 0;font-size: 22px;color: #fff;"><?=Yii::t('translate', 'serh')?></div>
        <div class="usluga__tabs">
            <div class="usluga__tab"><?=Yii::t('translate', 'sert1')?></div>
            <div class="usluga__tab"><?=Yii::t('translate', 'sert2')?></div>
        </div>
        <div class="company__hr"></div>
        <div class="usluga__contents">
            <div class="usluga__content">
                <div class="company__text">
                    <div class="usluga__header"><?=Yii::t('translate', 'serit1h')?></div>
                    <?=Yii::t('translate', 'serit1t')?>
                </div>
                <div class="usluga__steps">
                    <div class="usluga__step">
                        <div class="usluga__stepNumber"><?=Yii::t('translate', 'serst1')?></div>
                        <div class="usluga__stepHeader"><?=Yii::t('translate', 'serst1h')?></div>
                        <div class="usluga__stepText"><?=Yii::t('translate', 'serst1t')?></div>
                    </div>
                    <div class="usluga__step">
                        <div class="usluga__stepNumber"><?=Yii::t('translate', 'serst2')?></div>
                        <div class="usluga__stepHeader"><?=Yii::t('translate', 'serst2h')?></div>
                        <div class="usluga__stepText"><?=Yii::t('translate', 'serst2t')?></div>
                    </div>
                    <div class="usluga__step">
                        <div class="usluga__stepNumber"><?=Yii::t('translate', 'serst3')?></div>
                        <div class="usluga__stepHeader"><?=Yii::t('translate', 'serst3h')?></div>
                        <div class="usluga__stepText"><?=Yii::t('translate', 'serst3t')?></div>
                    </div>
                </div>
            </div>
            <div class="company__hr"></div>
            <div class="usluga__content">
                <div class="company__text">
                    <div class="usluga__header"><?=Yii::t('translate', 'serit2h')?></div>
                    <?=Yii::t('translate', 'serit2t')?>
                </div>
                <div class="usluga__steps">
                    <div class="usluga__step">
                        <div class="usluga__stepNumber"><?=Yii::t('translate', 'serst1')?></div>
                        <div class="usluga__stepHeader"><?=Yii::t('translate', 'serst1h')?></div>
                        <div class="usluga__stepText"><?=Yii::t('translate', 'serst1t')?></div>
                    </div>
                    <div class="usluga__step">
                        <div class="usluga__stepNumber"><?=Yii::t('translate', 'serst2')?></div>
                        <div class="usluga__stepHeader"><?=Yii::t('translate', 'serst2h')?></div>
                        <div class="usluga__stepText"><?=Yii::t('translate', 'serst2t')?></div>
                    </div>
                    <div class="usluga__step">
                        <div class="usluga__stepNumber"><?=Yii::t('translate', 'serst3')?></div>
                        <div class="usluga__stepHeader"><?=Yii::t('translate', 'serst3h')?></div>
                        <div class="usluga__stepText"><?=Yii::t('translate', 'serst3t')?></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="usluga__text"><?=Yii::t('translate', 'serbottom')?></div>
        <div class="company__hr"></div>
        <?=$this->render('@frontend/views/blocks/mainForm', ['class' => ' mainForm_company'])?>
        <br>
        <br>
    </div>
</div>
