<?php
if (Yii::$app->language == 'en') {
    $seoTitle = $model->en_seo_title;
    $seoDescription = $model->en_seo_description;
    $seoKeywords = $model->en_seo_keywords;
    $seoH1 = $model->en_seo_h1;

    $name = $model->en_name;
} else {
    $seoTitle = $model->seo_title;
    $seoDescription = $model->seo_description;
    $seoKeywords = $model->seo_keywords;
    $seoH1 = $model->seo_h1;

    $name = $model->name;
}
?>
<div class="mainSection">
    <div class="container">
        <div class="header"><span><?=(empty($seoH1)) ? $name : $seoH1 ?></span></div>
        <div class="light text-center" style="font-size: 18px;color: #fff;">
            <p>Мы всегда рады видеть в нашем коллективе профессионалов<br>
                в различных сферах рынка интернет-маркетинга и специалистов<br>
                других направлений — квалифицированных, целеустремлённых,<br>
                энергичных, уверенных в своём успехе людей.</p>
            <p>Мы ждём в нашу команду молодых специалистов, обладающих<br>
                профессиональным и личным потенциалом и желанием<br>
                развиваться вместе с нами.</p>
        </div>
        <div class="vacancy">
            <div class="vacancy__header">SEO-оптимизатор</div>
            <div class="vacancy__inner">
                <div class="vacancy__item">
                    <div class="vacancy__itemHeader">Должностные обязанности:</div>
                    <ul>
                        <li>Знание html, понимание принципов
                            работы php, умение разобраться в коде;</li>
                        <li>Аудит сайтов, написание рекомендаций
                            и ТЗ по сайтам, текстам;</li>
                        <li>Оптимизация сайта;</li>
                        <li>Закупка и зачистка ссылок;</li>
                        <li>Умение вести более одного проекта одновременно.</li>
                    </ul>
                </div>
                <div class="vacancy__item">
                    <div class="vacancy__itemHeader">Зароботная плата: </div>
                    <ul>
                        <li>от 40 000 р. до 120 000 р.</li>
                    </ul>
                    <div class="vacancy__itemHeader" style="margin-top: 45px;">Мы предлагаем:</div>
                    <ul>
                        <li>График работы 5/2;</li>
                        <li>Молодой дружный коллектив;</li>
                        <li>Карьерный рост;</li>
                        <li>Профессиональный опыт.</li>
                    </ul>
                </div>
                <div class="vacancy__item">
                    <div class="vacancy__itemHeader">Личные качества:</div>
                    <ul>
                        <li>Активность, коммуникабельность, ответственность;</li>
                        <li>Опыт работы в аналогичной должности
                            не менее 1 года;</li>
                        <li>Нацеленность на результат;</li>
                        <li>Грамотность, усидчивость;</li>
                        <li>Умение брать на себя ответственность
                            за действия и результат.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
