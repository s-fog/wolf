<?php
use common\models\Client;

if (Yii::$app->language == 'en') {
    $seoTitle = $model->en_seo_title;
    $seoDescription = $model->en_seo_description;
    $seoKeywords = $model->en_seo_keywords;
    $seoH1 = $model->en_seo_h1;

    $name = $model->en_name;
} else {
    $seoTitle = $model->seo_title;
    $seoDescription = $model->seo_description;
    $seoKeywords = $model->seo_keywords;
    $seoH1 = $model->seo_h1;

    $name = $model->name;
}

?>
<div class="mainSection company">
    <div class="container">
        <div class="header"><span><?=(empty($seoH1)) ? $name : $seoH1 ?></span></div>
        <div class="clients__inner">
            <?php foreach(Client::find()->orderBy(['sortOrder' => SORT_ASC])->all() as $client) {
                if (Yii::$app->language == 'en') {
                    $name = $client->name;
                    $introtext = $client->en_introtext;
                    $text = $client->en_text;
                    $queries = $client->en_queries;
                } else {
                    $name = $client->name;
                    $introtext = $client->introtext;
                    $text = $client->text;
                    $queries = $client->queries;
                }
                ?>
                <div class="clients__item">
                    <a href="http://<?=$name?>" target="_blank" class="clients__itemImage" style="background-image: url(<?=$client->image?>);"></a>
                    <a href="http://<?=$name?>" target="_blank" class="clients__itemName"><span><?=$name?></span></a>
                    <div class="clients__itemText"><?=$introtext?></div>
                </div>
            <?php } ?>
        </div>
        <br>
        <br>
        <br>
        <br>
        <div class="company__hr"></div>
        <?=$this->render('@frontend/views/blocks/mainForm', ['class' => ' mainForm_company'])?>
        <br>
        <br>
    </div>
</div>
