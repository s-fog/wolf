<?php
if (Yii::$app->language == 'en') {
    $seoTitle = $model->en_seo_title;
    $seoDescription = $model->en_seo_description;
    $seoKeywords = $model->en_seo_keywords;
    $seoH1 = $model->en_seo_h1;

    $name = $model->en_name;
} else {
    $seoTitle = $model->seo_title;
    $seoDescription = $model->seo_description;
    $seoKeywords = $model->seo_keywords;
    $seoH1 = $model->seo_h1;

    $name = $model->name;
}
?>
<div class="map" onselectstart="return false" onmousedown="return false">
    <div id="map"></div>
    <div class="header">
        <div class="container">
            <span><?=(empty($seoH1)) ? $name : $seoH1 ?></span>
        </div>
    </div>
    <?=$this->render('@frontend/views/blocks/contactsMap')?>
</div>
<?=$this->render('@frontend/views/blocks/mainForm')?>
<?=$this->render('@frontend/views/blocks/mapTop', ['class' => 'map__top map__top_2'])?>
