<?php

if (Yii::$app->language == 'en') {
    $seoTitle = $model->en_seo_title;
    $seoDescription = $model->en_seo_description;
    $seoKeywords = $model->en_seo_keywords;
    $seoH1 = $model->en_seo_h1;

    $name = $model->en_name;
} else {
    $seoTitle = $model->seo_title;
    $seoDescription = $model->seo_description;
    $seoKeywords = $model->seo_keywords;
    $seoH1 = $model->seo_h1;

    $name = $model->name;
}
?>
<div class="mainSection company">
    <div class="container">
        <div class="header"><span><?=(empty($seoH1)) ? $name : $seoH1 ?></span></div>
        <div class="company__text">
            <?=Yii::t('translate', 'aboutContent')?>
        </div>
        <div class="company__hr"></div>
        <div class="company__faces">
            <div class="company__face">
                <div class="company__faceImage" style="background-image: url(/img/face1.png);"></div>
                <div class="company__faceName">Екатерина Власова</div>
                <div class="company__faceFunction">Project-manager</div>
            </div>
            <div class="company__face">
                <div class="company__faceImage" style="background-image: url(/img/face2.png);"></div>
                <div class="company__faceName">Александр Пронин </div>
                <div class="company__faceFunction">Project-менеджер</div>
            </div>
            <div class="company__face">
                <div class="company__faceImage" style="background-image: url(/img/face3.png);"></div>
                <div class="company__faceName">Ольга Короленко</div>
                <div class="company__faceFunction">SEO-оптимизатор</div>
            </div>
            <div class="company__face">
                <div class="company__faceImage" style="background-image: url(/img/face4.png);"></div>
                <div class="company__faceName">Петр Коротков</div>
                <div class="company__faceFunction">SEO-оптимизатор</div>
            </div>
            <div class="company__face">
                <div class="company__faceImage" style="background-image: url(/img/face5.png);"></div>
                <div class="company__faceName">Максим Прохоров</div>
                <div class="company__faceFunction">SEO-оптимизатор</div>
            </div>
            <div class="company__face">
                <div class="company__faceImage" style="background-image: url(/img/face6.png);"></div>
                <div class="company__faceName">Вакансия </div>
                <div class="company__faceFunction">SEO-оптимизатор</div>
            </div>
            <div class="company__face">
                <div class="company__faceImage"></div>
                <div class="company__faceName">Сергей Стригунков </div>
                <div class="company__faceFunction">Генеральный директор</div>
            </div>
            <div class="company__face">
                <div class="company__faceImage"></div>
                <div class="company__faceName">Никита Морозенко</div>
                <div class="company__faceFunction">Исполнительный директор</div>
            </div>
        </div>
        <div class="company__hr"></div>
        <div class="adv">
            <div class="adv__inner">
                <div class="adv__item">
                    <div class="adv__itemImage" style="background-image: url(/img/aaa1.png);"></div>
                    <div class="adv__itemHeader"><?=Yii::t('translate', 'adv1h')?></div>
                    <div class="adv__itemText"><?=Yii::t('translate', 'adv1t')?></div>
                </div>
                <div class="adv__item">
                    <div class="adv__itemImage" style="background-image: url(/img/aaa2.png);"></div>
                    <div class="adv__itemHeader"><?=Yii::t('translate', 'adv2h')?></div>
                    <div class="adv__itemText"><?=Yii::t('translate', 'adv2t')?></div>
                </div>
                <div class="adv__item">
                    <div class="adv__itemImage" style="background-image: url(/img/aaa3.png);"></div>
                    <div class="adv__itemHeader"><?=Yii::t('translate', 'adv3h')?></div>
                    <div class="adv__itemText"><?=Yii::t('translate', 'adv3t')?></div>
                </div>
                <div class="adv__item">
                    <div class="adv__itemImage" style="background-image: url(/img/aaa4.png);"></div>
                    <div class="adv__itemHeader"><?=Yii::t('translate', 'adv4h')?></div>
                    <div class="adv__itemText"><?=Yii::t('translate', 'adv4t')?></div>
                </div>
                <div class="adv__item">
                    <div class="adv__itemImage" style="background-image: url(/img/aaa5.png);"></div>
                    <div class="adv__itemHeader"><?=Yii::t('translate', 'adv5h')?></div>
                    <div class="adv__itemText"><?=Yii::t('translate', 'adv5t')?></div>
                </div>
                <div class="adv__item">
                    <div class="adv__itemImage" style="background-image: url(/img/aaa6.png);"></div>
                    <div class="adv__itemHeader"><?=Yii::t('translate', 'adv6h')?></div>
                    <div class="adv__itemText"><?=Yii::t('translate', 'adv6t')?></div>
                </div>
            </div>
        </div>
        <div class="company__hr"></div>
        <?=$this->render('@frontend/views/blocks/mainForm', ['class' => ' mainForm_company'])?>
        <br>
        <br>
    </div>
</div>
