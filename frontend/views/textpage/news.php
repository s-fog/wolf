<?php
use common\models\News;
use yii\imagine\Image;

if (Yii::$app->language == 'en') {
    $seoTitle = $model->en_seo_title;
    $seoDescription = $model->en_seo_description;
    $seoKeywords = $model->en_seo_keywords;
    $seoH1 = $model->en_seo_h1;

    $name = $model->en_name;
} else {
    $seoTitle = $model->seo_title;
    $seoDescription = $model->seo_description;
    $seoKeywords = $model->seo_keywords;
    $seoH1 = $model->seo_h1;

    $name = $model->name;
}
?>
<div class="mainSection">
    <div class="container">
        <div class="header"><span><?=(empty($seoH1)) ? $name : $seoH1 ?></span></div>
        <div class="news__inner">
            <?php foreach($news as $news_item) {
                if (Yii::$app->language == 'en') {
                    $name = $news_item->en_name;
                    $introtext = $news_item->en_introtext;
                } else {
                    $name = $news_item->name;
                    $introtext = $news_item->introtext;
                }
                ?>
                <a href="<?=\yii\helpers\Url::to(['news/index', 'alias' => $news_item->alias])?>" class="news__item">
                    <?php
                    $filename = explode('.', basename($news_item->image));
                    $thumbPath = '/uploads/thumbs/'.$filename[0].'-278-278.jpg';
                    Image::thumbnail('@www' . $news_item->image, 278, 278)
                        ->save(Yii::getAlias('@www'.$thumbPath), ['quality' => 80]);
                    ?>
                    <span class="news__itemLeft" style="background-image: url(<?=$thumbPath?>);"></span>
                    <span class="news__itemRight">
                        <span class="news__itemDate"><?=date('d.m.Y', $news_item->date)?></span>
                        <span class="news__itemHeader"><?=$name?></span>
                        <span class="news__itemText"><?=$introtext?></span>
                    </span>
                </a>
            <?php } ?>
        </div>
        <?=News::pagination(News::find()->count(), $page, $limit)?>
    </div>
</div>
