<?php

use common\models\Client;
use common\models\Mainpage;

$mainPage = Mainpage::findOne(1);

if (Yii::$app->language == 'en') {
    $seoTitle = $mainPage->en_seo_title;
    $seoDescription = $mainPage->en_seo_description;
    $seoKeywords = $mainPage->en_seo_keywords;
} else {
    $seoTitle = $mainPage->seo_title;
    $seoDescription = $mainPage->seo_description;
    $seoKeywords = $mainPage->seo_keywords;
}

$this->params['seo_title'] = $seoTitle;
$this->params['seo_description'] = $seoDescription;
$this->params['seo_keywords'] = $seoKeywords;

?>
<div class="mainSlider">
    <div class="mainSlider__inner owl-carousel">
        <div class="mainSlider__item" style="background-image: url(/img/slide.png);">
            <div class="container">
                <div class="mainSlider__itemInner">
                    <div class="mainSlider__itemText1"><?=Yii::t('translate', 'sl1t1')?></div>
                    <div class="mainSlider__itemText2"><?=Yii::t('translate', 'sl1t2')?></div>
                    <div class="mainSlider__itemHeader"><?=Yii::t('translate', 'sl1h')?></div>
                </div>
                <div class="mainSlider__itemText3"><?=Yii::t('translate', 'sl1t3')?></div>
                <div class="mainSlider__itemLinks">
                    <span class="mainSlider__itemLink" style="background-image: url(/img/yandex.png);width: 115px;height: 46px;"></span>
                    <span class="mainSlider__itemLink" style="position: relative;top:3px;background-image: url(/img/google.png);width: 134px;height: 44px;"></span>
                </div>
            </div>
        </div>
        <div class="mainSlider__item" style="background-image: url(/img/sl2.png);">
            <div class="container">
                <div class="mainSlider__itemInner" style="padding-top: 50px;">
                    <div class="mainSlider__itemText1" style="width: 90%;font-size: 22px;">Результативность наших клиентов -<br>
                        главный показатель эффективности работы WOLF
                    </div>
                    <div class="mainSlider__itemText2">
                        Любой крупный бизнес в сфере товаров или услуг<br>
                        не может существовать без удобного эффективного сайта.<br>
                        Таковы сегодняшние реалии. Но при всех своих удобствах<br>
                        сайт нуждается в постоянном потоке клиентов. </div>
                    <div class="mainSlider__itemHeader" style="margin-top: -5px;font-size: 62px;">историю пишут победители</div>
                    <div class="mainSlider__itemText2" style="margin-top: 105px;">Команда Wolf.ru использует ряд уникальных методик<br>
                        для вывода сайта в топ Yandex и Google.</div>
                </div>
                <div class="mainSlider__itemText3">Эффективные методы продвижения в поисковых системах</div>
                <div class="mainSlider__itemLinks">
                    <span class="mainSlider__itemLink" style="background-image: url(/img/yandex.png);width: 115px;height: 46px;"></span>
                    <span class="mainSlider__itemLink" style="position: relative;top:3px;background-image: url(/img/google.png);width: 134px;height: 44px;"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="underMainSlider">
        <div class="container">
            <div class="underMainSlider__inner">
                <div class="underMainSlider__top">не прячемся за умными словами<br>
                    и работаем максимально прозрачно</div>
                <div class="underMainSlider__bottom">Оценить результаты очень просто. Ниже представлен перечень наших клиентов и ключевые слова,<br>
                    по которым сайт находится в top-10. Просто нажмите на запрос, и вы все увидите сами.</div>
                <div class="underMainSlider__arrow"></div>
            </div>
        </div>
    </div>
</div>
<div class="yandex">
    <div class="yandex__inner owl-carousel">
        <?php foreach(Client::find()->orderBy(['sortOrder' => SORT_ASC])->all() as $model) {
            if (Yii::$app->language == 'en') {
                $name = $model->name;
                $introtext = $model->en_introtext;
                $text = $model->en_text;
                $queries = $model->en_queries;
            } else {
                $name = $model->name;
                $introtext = $model->introtext;
                $text = $model->text;
                $queries = $model->queries;
            }
            ?>

            <div class="yandex__item">
                <div class="yandex__top">
                    <div class="container">
                        <a href="http://<?=$name?>" target="_blank" class="yandex__topLeft" style="background-image: url(<?=$model->image?>);"></a>
                        <div class="yandex__topRight">
                            <div class="yandex__topYear"><?=$introtext?></div>
                            <a href="http://<?=$name?>" target="_blank" class="yandex__topHeader"><?=$name?></a>
                            <div class="yandex__topText"><?=$text?></div>
                        </div>
                    </div>
                </div>
                <div class="yandex__middle">
                    <div class="container">
                        <div class="yandex__middleLeft">ТОП-10</div>
                        <div class="yandex__middleRight">Данный сайт находится в top-10 yandex.ru по запросам:</div>
                    </div>
                </div>
                <?php if (!empty($queries)) { ?>
                    <div class="yandex__bottom">
                        <div class="container">
                            <?php
                            foreach(explode(';', $queries) as $query) {
                                $q = explode('_', $query);?>
                                <div class="yandex__bottomItem">
                                    <div class="yandex__bottomItemTop"><?=$q[0]?></div>
                                    <div class="yandex__bottomItemBottom">Запрос находится на <a href="https://yandex.ru/search/?text=<?=$q[0]?>" target="_blank"><?=$q[1]?> месте</a> yandex.ru</div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
</div>
<div class="steps">
    <div class="container">
        <div class="steps__header"><?=Yii::t('translate', 'stepsHeader')?></div>
        <div class="steps__inner">
            <div class="steps__item"><?=Yii::t('translate', 'step1')?></div>
            <div class="steps__item"><?=Yii::t('translate', 'step2')?></div>
            <div class="steps__item"><?=Yii::t('translate', 'step3')?></div>
            <div class="steps__item"><?=Yii::t('translate', 'step4')?></div>
            <div class="steps__item"><?=Yii::t('translate', 'step5')?></div>
            <div class="steps__item"><?=Yii::t('translate', 'step6')?></div>
        </div>
    </div>
</div>
<div class="advantages">
    <div class="container">
        <div class="advantages__inner">
            <div class="advantages__item">
                <div class="advantages__left" style="background-image: url(/img/a1.png);"></div>
                <div class="advantages__right">
                    <div class="advantages__header"><?=Yii::t('translate', 'adv1h')?></div>
                    <div class="advantages__text"><?=Yii::t('translate', 'adv1t')?></div>
                </div>
            </div>
            <div class="advantages__item">
                <div class="advantages__left" style="background-image: url(/img/a2.png);"></div>
                <div class="advantages__right">
                    <div class="advantages__header"><?=Yii::t('translate', 'adv2h')?></div>
                    <div class="advantages__text"><?=Yii::t('translate', 'adv2t')?></div>
                </div>
            </div>
            <div class="advantages__item">
                <div class="advantages__left" style="background-image: url(/img/a3.png);"></div>
                <div class="advantages__right">
                    <div class="advantages__header"><?=Yii::t('translate', 'adv3h')?></div>
                    <div class="advantages__text"><?=Yii::t('translate', 'adv3t')?></div>
                </div>
            </div>
            <div class="advantages__item">
                <div class="advantages__left" style="background-image: url(/img/a4.png);"></div>
                <div class="advantages__right">
                    <div class="advantages__header"><?=Yii::t('translate', 'adv4h')?></div>
                    <div class="advantages__text"><?=Yii::t('translate', 'adv4t')?></div>
                </div>
            </div>
            <div class="advantages__item">
                <div class="advantages__left" style="background-image: url(/img/a5.png);"></div>
                <div class="advantages__right">
                    <div class="advantages__header"><?=Yii::t('translate', 'adv5h')?></div>
                    <div class="advantages__text"><?=Yii::t('translate', 'adv5t')?></div>
                </div>
            </div>
            <div class="advantages__item">
                <div class="advantages__left" style="background-image: url(/img/a6.png);"></div>
                <div class="advantages__right">
                    <div class="advantages__header"><?=Yii::t('translate', 'adv6h')?></div>
                    <div class="advantages__text"><?=Yii::t('translate', 'adv6t')?></div>
                </div>
            </div>
        </div>
    </div>
</div>

<?=$this->render('@frontend/views/blocks/mainForm.php')?>

<div class="map" onselectstart="return false" onmousedown="return false">
    <div id="map"></div>
    <?=$this->render('@frontend/views/blocks/mapTop', ['class' => 'map__top'])?>
    <?=$this->render('@frontend/views/blocks/contactsMap')?>
</div>