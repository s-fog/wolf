<div class="map__content">
    <div class="container">
        <span class="contacts__address">г. Москва, ул. Тимура Фрунзе, 16с3</span>
        <br>
        <a href="tel:8 985 766-57-10" class="contacts__phone">8 985 766-57-10</a>
        <br>
        <a href="mailto:info@wolf.ru" class="contacts__email">info@wolf.ru</a>
        <br>
        <div class="button1" data-fancybox="" data-src="#callback"><?=Yii::t('translate', 'Order calculation')?></div>
        <br>
        <a href="#" class="bottomHeader__presentation"><span><?=Yii::t('translate', 'Agency presentation')?></span></a>
    </div>
</div>