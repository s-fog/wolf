<?php
if (!isset($class)) {
    $class = '';
}
?>
<form class="mainForm sendForm<?=$class?>">
    <div class="container">
        <div class="mainForm__header"><?=Yii::t('translate', 'mainFormHeader')?></div>
        <div class="mainForm__text"><?=Yii::t('translate', 'mainFormText')?></div>
        <div class="mainForm__inner">
            <div class="form-group mainForm__address"><input type="text" name="address" class="input required" placeholder="<?=Yii::t('translate', 'Site address')?> *"></div>
            <div class="form-group"><input type="text" name="name" class="input" placeholder="<?=Yii::t('translate', 'Name')?>"></div>
            <div class="form-group"><input type="text" name="email" class="input" placeholder="<?=Yii::t('translate', 'Email')?>"></div>
            <div class="form-group"><input type="text" name="phone" class="input required" placeholder="<?=Yii::t('translate', 'Phone')?> *"></div>
        </div>
        <div class="mainForm__requiredText"><?=Yii::t('translate', 'requireFields')?></div>
        <hr>
        <button class="mainForm__submit button2" type="submit"><?=Yii::t('translate', 'takeRequest')?></button>
        <input type="hidden" name="type" value="Обратный звонок с сайта Wolf.ru">
        <input type="text" name="BC" class="BC">
    </div>
</form>