<?php
use common\models\Textpage;
?>
<div class="container">
    <div class="bottomHeader__inner">
        <a href="/pr_wolf.pdf" target="_blank" class="bottomHeader__presentation"><span><?=Yii::t('translate', 'Agency presentation')?></span></a>
        <ul class="bottomHeader__menu">
            <?=Textpage::liLink(4, 'bottomHeader__menuLink');?>
            <?=Textpage::liLink(5, 'bottomHeader__menuLink');?>
            <?=Textpage::liLink(6, 'bottomHeader__menuLink');?>
        </ul>
        <div class="socials">
            <a href="#" class="socials__item" style="background-image: url(/img/vk.png);width: 52px;height: 52px;"></a>
            <a href="#" class="socials__item" style="background-image: url(/img/facebook.png);width: 52px;height: 52px;"></a>
            <a href="#" class="socials__item" style="background-image: url(/img/insta.png);width: 52px;height: 52px;"></a>
        </div>
    </div>
</div>