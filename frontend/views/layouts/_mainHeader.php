<?php
use common\models\Textpage;
?>
<div class="mainHeader">
    <div class="container">
        <div class="mainHeader__inner">
            <a href="/" class="logo">
                SEO-оптимизация
                WEB аналитика
                Продвижение сайтов
            </a>
            <ul class="menu1">
                <?=Textpage::liLink(1, 'menu1__link');?>
                <?=Textpage::liLink(2, 'menu1__link');?>
                <?=Textpage::liLink(3, 'menu1__link');?>
            </ul>
            <div class="button1" data-fancybox="" data-src="#callback"><?=Yii::t('translate', 'Order calculation')?></div>
            <div class="contacts">
                <a href="tel:8 985 766-57-10" class="contacts__phone">8 985 766-57-10</a>
                <a href="#" class="contacts__address">г. Москва, ул. Тимура Фрунзе, 16с3</a>
            </div>
        </div>
    </div>
</div>