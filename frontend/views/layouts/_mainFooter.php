<?php
use common\models\Textpage;
?>
<div class="mainFooter">
    <div class="container">
        <div class="mainFooter__inner">
            <div class="mainFooter__left">
                <a href="/" class="logo">
                    SEO-оптимизация
                    WEB аналитика
                    Продвижение сайтов
                </a>
                <div class="mainFooter__leftText">
                    © 2008–2017. Wolf.ru Все права защищены.<br>
                    Данный сайт носит информационно-справочный характер.<br>
                    Не является публичной офертой.
                </div>
            </div>
            <div class="mainFooter__item">
                <ul class="mainFooter__menu">
                    <?=Textpage::liLink(1, 'mainFooter__menuLink');?>
                    <?=Textpage::liLink(2, 'mainFooter__menuLink');?>
                    <?=Textpage::liLink(3, 'mainFooter__menuLink');?>
                </ul>
            </div>
            <div class="mainFooter__item">
                <ul class="mainFooter__menu2">
                    <?=Textpage::liLink(4, 'mainFooter__menu2Link');?>
                    <?=Textpage::liLink(5, 'mainFooter__menu2Link');?>
                    <?=Textpage::liLink(6, 'mainFooter__menu2Link');?>
                </ul>
            </div>
            <div class="mainFooter__right">
                <a href="tel:8 985 766-57-10" class="contacts__phone">8 985 766-57-10</a>
                <span class="mainFooter__rightText">г. Москва, ул. Тимура Фрунзе, 16с3</span>
                <span class="mainFooter__rightText">info@wolf.ru</span>
                <div class="mainFooter__socials">
                    <div class="socials">
                        <a href="#" class="socials__item" style="background-image: url(/img/vk2.png);width: 52px;height: 52px;"></a>
                        <a href="#" class="socials__item" style="background-image: url(/img/facebook2.png);width: 52px;height: 52px;"></a>
                        <a href="#" class="socials__item" style="background-image: url(/img/insta2.png);width: 52px;height: 52px;"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="callback" class="popup">
    <a href="/" class="logo">
        SEO-оптимизация
        WEB аналитика
        Продвижение сайтов
    </a>
    <?=$this->render('@frontend/views/blocks/mainForm.php')?>
</div>
<div id="success" class="popup">
    <a href="/" class="logo">
        SEO-оптимизация
        WEB аналитика
        Продвижение сайтов
    </a>
    <div style="font-size: 36px;">Спасибо, что оставили заявку</div>
    <div style="font-size: 24px;">Менеджеры компании свяжутся с вами в самое<br>ближайшее время.</div>
</div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAvEkvEmbhD2IVVkrUjJQlKdEEZmKWUvFY"></script>