<?php

if (Yii::$app->language == 'en') {
    $seoTitle = $model->en_seo_title;
    $seoDescription = $model->en_seo_description;
    $seoKeywords = $model->en_seo_keywords;
    $seoH1 = $model->en_seo_h1;

    $name = $model->en_name;
    $text = $model->en_text;
} else {
    $seoTitle = $model->seo_title;
    $seoDescription = $model->seo_description;
    $seoKeywords = $model->seo_keywords;
    $seoH1 = $model->seo_h1;

    $name = $model->name;
    $text = $model->text;
}
?>
<div class="mainSection">
    <div class="container">
        <div class="header"><span><?=(empty($seoH1)) ? $name : $seoH1 ?></span></div>
        <div class="news__item news__item_unhover">
            <div class="news__itemLeft news__itemLeft_big" style="background-image: url(<?=$model->image?>);"></div>
            <div class="news__itemRight">
                <div class="news__itemDate"><?=date('d.m.Y', $model->date)?></div>
                <div class="news__itemHeader"><?=$name?></div>
                <div class="news__itemText"><?=$text?></div>
            </div>
        </div>
    </div>
</div>
