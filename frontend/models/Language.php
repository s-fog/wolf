<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class Language extends Model
{
    public static function start() {
        $session = Yii::$app->session;

        if (empty($session->get('_language'))) {
            preg_match_all('/([a-z]{1,8}(?:-[a-z]{1,8})?)(?:;q=([0-9.]+))?/', strtolower($_SERVER["HTTP_ACCEPT_LANGUAGE"]), $matches); // вычисляем соответствия с массивом $matches
            $langs = array_combine($matches[1], $matches[2]);
            foreach ($langs as $n => $v)
                $langs[$n] = $v ? $v : 1;
            arsort($langs);

            if (strstr(key($langs), 'ru')) {
                $session->set('_language', 'ru');
            } else {
                $session->set('_language', 'en');
            }
        }

        Yii::$app->language = $session->get('_language');
    }
}
