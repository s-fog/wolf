<?php
namespace frontend\controllers;

use common\models\News;
use common\models\Textpage;
use Yii;
use yii\web\Controller;

/**
 * Site controller
 */
class TextpageController extends BaseController
{
    public function actionIndex($alias)
    {
        $this->layout = 'textpage';
        $view = '';
        $model = Textpage::find()->where(['alias' => $alias])->one();

        switch($model->id) {
            case 1: {
                $page = (!empty($_GET['page']))? $_GET['page']: 1;
                $limit = 12;
                $offset = ($page == 1)? 0 : $page * $limit - $limit;
                $news = News::find()
                    ->limit($limit)
                    ->offset($offset)
                    ->orderBy(['date' => SORT_DESC])
                    ->all();

                return $this->render('news', [
                    'model' => $model,
                    'news' => $news,
                    'page' => $page,
                    'limit' => $limit,
                ]);
                break;
            }
            case 2: $view = 'vacancy';break;
            case 3: $view = 'contacts';break;
            case 4: $view = 'company';break;
            case 5: $view = 'services';break;
            case 6: $view = 'clients';break;
        }

        return $this->render($view, ['model' => $model]);
    }
}
