<?php
namespace frontend\controllers;

use common\models\News;
use Yii;
use yii\web\Controller;


class NewsController extends BaseController
{
    public function actionIndex($alias)
    {
        $this->layout = 'textpage';
        $model = News::find()->where(['alias' => $alias])->one();
        return $this->render('index', ['model' => $model]);
    }
}
