<?php

namespace frontend\controllers;

use common\models\Review;
use frontend\models\Language;
use Yii;
use yii\web\Controller;

class BaseController extends Controller
{
    public function beforeAction($action)
    {
        Language::start();
        return parent::beforeAction($action);
    }
}
