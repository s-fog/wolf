<?php

namespace backend\controllers\api;

/**
* This is the class for REST controller "MainpageController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class MainpageController extends \yii\rest\ActiveController
{
public $modelClass = 'common\models\Mainpage';
}
