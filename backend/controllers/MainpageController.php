<?php

namespace backend\controllers;

use yii\filters\AccessControl;

/**
* This is the class for controller "MainpageController".
*/
class MainpageController extends \backend\controllers\base\MainpageController
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

}
