<?php

namespace backend\controllers;

use common\models\Client;
use himiklab\sortablegrid\SortableGridAction;
use yii\filters\AccessControl;

/**
* This is the class for controller "ClientController".
*/
class ClientController extends \backend\controllers\base\ClientController
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'sort' => [
                'class' => SortableGridAction::className(),
                'modelName' => Client::className(),
            ],
        ];
    }

    public function actionCreate()
    {
        $model = new Client;

        if (!empty($_POST['Client']['queries'])) {
            $_POST['Client']['queries'] = implode(';', $_POST['Client']['queries']);
        }

        if (!empty($_POST['Client']['en_queries'])) {
            $_POST['Client']['en_queries'] = implode(';', $_POST['Client']['en_queries']);
        }

        try {
            if ($model->load($_POST) && $model->save()) {
                if ($_POST['mode'] == 'justSave') {
                    return $this->redirect(['update', 'id' => $model->id]);
                } else {
                    return $this->redirect(['index']);
                }
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
        }
        return $this->render('create', ['model' => $model]);
    }

    /**
     * Updates an existing Client model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (!empty($_POST['Client']['queries'])) {
            $_POST['Client']['queries'] = implode(';', $_POST['Client']['queries']);
        }

        if (!empty($_POST['Client']['en_queries'])) {
            $_POST['Client']['en_queries'] = implode(';', $_POST['Client']['en_queries']);
        }

        if ($model->load($_POST) && $model->save()) {
            if ($_POST['mode'] == 'justSave') {
                return $this->redirect(['update', 'id' => $model->id]);
            } else {
                return $this->redirect(['index']);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
}
