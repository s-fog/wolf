<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var common\models\News $model
*/
    
$this->title = Yii::t('models', 'News') . " " . $model->name . ', ' . 'Edit';
$this->params['breadcrumbs'][] = ['label' => Yii::t('models', 'News'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="giiant-crud news-update">

    <h1>
        <?= Yii::t('models', 'News') ?>
        <small>
                        <?= $model->name ?>
        </small>
    </h1>

    <hr />

    <?php echo $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>
