<?php

?>
<br>
<?= $form->field($model, 'en_name')->textInput(['maxlength' => true]) ?>

    <!-- attribute introtext -->
<?= $form->field($model, 'en_introtext')->textInput(['maxlength' => true]) ?>

    <!-- attribute text -->
<?= $form->field($model, 'en_text')->textarea(['rows' => 6]) ?>

    <!-- attribute seo_description -->
<?= $form->field($model, 'en_seo_description')->textarea(['rows' => 6]) ?>

    <!-- attribute seo_title -->
<?= $form->field($model, 'en_seo_title')->textInput(['maxlength' => true]) ?>

    <!-- attribute seo_keywords -->
<?= $form->field($model, 'en_seo_keywords')->textInput(['maxlength' => true]) ?>

    <!-- attribute seo_h1 -->
<?= $form->field($model, 'en_seo_h1')->textInput(['maxlength' => true]) ?>