<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var common\models\News $model
*/

$this->title = 'Создать';
$this->params['breadcrumbs'][] = ['label' => Yii::t('models', 'News'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="giiant-crud news-create">

    <h1>
        <?= Yii::t('models', 'News') ?>
        <small>
                        <?= $model->name ?>
        </small>
    </h1>

    <hr />

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>
