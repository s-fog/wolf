<?php

use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\StringHelper;
use mihaildev\elfinder\InputFile;
use xtarantulz\preview\PreviewAsset;
/**
 * @var yii\web\View $this
 * @var common\models\Client $model
 * @var yii\widgets\ActiveForm $form
 */
PreviewAsset::register($this);
?>

<div class="news-form">

    <?php $form = ActiveForm::begin([
    'id' => 'News',
    'layout' => 'horizontal',
    'enableClientValidation' => true,
    'errorSummaryCssClass' => 'error-summary alert alert-danger'
    ]
    );
    ?>
    <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'image')->widget(InputFile::className(), [
        'language'      => 'ru',
        'controller'    => 'elfinder', // вставляем название контроллера, по умолчанию равен elfinder
        'filter'        => 'image',    // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
        'template'      => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
        'options'       => ['class' => 'form-control img'],
        'buttonOptions' => ['class' => 'btn btn-success'],
        'multiple'      => false       // возможность выбора нескольких файлов
    ]) ?>

    <?=$form->field($model, 'date')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'Выберите дату ...'],
        'pluginOptions' => [
            'format' => 'dd.mm.yyyy',
            'autoclose'=>true,
            'todayHighlight' => false
        ]
    ]);?>

    <?=Tabs::widget([
        'items' => [
            [
                'label'     =>  'RU',
                'content'   =>  $this->render('_ru', ['form' => $form, 'model' => $model]),
                'active'    =>  true
            ],
            [
                'label'     => 'EN',
                'content'   =>  $this->render('_en', ['form' => $form, 'model' => $model])
            ]
        ]
    ]);?>
        <hr/>

        <?php echo $form->errorSummary($model); ?>

        <?= Html::submitButton(
            '<span class="glyphicon glyphicon-check"></span> ' .
            ($model->isNewRecord ? 'Создать' : 'Сохранить'),
            [
                'id' => 'save-' . $model->formName(),
                'class' => 'btn btn-success',
                'name' => 'mode',
                'value' => 'justSave'
            ]
        );
        ?>
        <?=Html::submitButton(
            '<span class="glyphicon glyphicon-check"></span> ' .
            ($model->isNewRecord ? 'Создать и выйти' : 'Сохранить и выйти'),
            [
                'class' => 'btn btn-success',
                'name' => 'mode',
                'value' => 'saveAndExit'
            ]
        );?>

        <?php ActiveForm::end(); ?>

    </div>

</div>

