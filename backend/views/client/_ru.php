<?php

use kartik\widgets\Select2;
?>
<br>
<?= $form->field($model, 'introtext')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

<?php
$data = [];
foreach(explode(';', $model->queries) as $value) {
    $data[$value] = $value;
}
$model->queries = explode(';', $model->queries);
?>

<?=$form->field($model, 'queries')->widget(Select2::classname(), [
    'data' => $data,
    'options' => ['placeholder' => 'Введите запросы', 'multiple' => true],
    'pluginOptions' => [
        'tags' => true,
        'tokenSeparators' => [',', ''],
        'maximumInputLength' => 100
    ],
]); ?>