<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
* @var yii\web\View $this
* @var common\models\ClientSearch $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="client-search">

    <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    ]); ?>

    		<?= $form->field($model, 'id') ?>

		<?= $form->field($model, 'sortOrder') ?>

		<?= $form->field($model, 'name') ?>

		<?= $form->field($model, 'link') ?>

		<?= $form->field($model, 'image') ?>

		<?php // echo $form->field($model, 'introtext') ?>

		<?php // echo $form->field($model, 'text') ?>

		<?php // echo $form->field($model, 'queries') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
