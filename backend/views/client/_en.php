<?php

use kartik\widgets\Select2;
?>
<br>
<?= $form->field($model, 'en_introtext')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'en_text')->textarea(['rows' => 6]) ?>

<?php
$data = [];
foreach(explode(';', $model->en_queries) as $value) {
    $data[$value] = $value;
}
$model->en_queries = explode(';', $model->en_queries);
?>

<?=$form->field($model, 'en_queries')->widget(Select2::classname(), [
    'data' => $data,
    'options' => ['placeholder' => 'Enter queries', 'multiple' => true],
    'pluginOptions' => [
        'tags' => true,
        'tokenSeparators' => [',', ''],
        'maximumInputLength' => 100
    ],
]); ?>
