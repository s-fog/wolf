<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var common\models\Client $model
*/
    
$this->title = Yii::t('models', 'Client') . " " . $model->name . ', ' . 'Edit';
$this->params['breadcrumbs'][] = ['label' => Yii::t('models', 'Client'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="giiant-crud client-update">

    <h1>
        <?= Yii::t('models', 'Client') ?>
        <small>
                        <?= $model->name ?>
        </small>
    </h1>

    <hr />

    <?php echo $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>
