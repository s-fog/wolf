<aside class="main-sidebar">

    <section class="sidebar">
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Главная страница', 'url' => ['/']],
                    ['label' => 'Клиенты', 'url' => ['/client']],
                    ['label' => 'Текстовые страницы', 'url' => ['/textpage']],
                    ['label' => 'Новости', 'url' => ['/news']],
                ],
            ]
        ) ?>

    </section>

</aside>
