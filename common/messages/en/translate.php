<?php

return [
    'sl1t1' => 'Hunting for clients - every<br>business\' favorite pursuit. ',
    'sl1t2' => 'We know the most effective way to manage<br>your site to achieve maximum results. ',
    'sl1t3' => 'Best practices in search engine optimization',
    'sl1h' => 'Hunting season is open.',
    'aboutHeader' => 'About the company',
    'aboutContent' => '<p>The company Wolf.ru was founded in 1997. Its main area of focus, which has not changed to this day, is search promotion. Back then, in 1997, Yandex didn\'t even exist yet. SEO and website optimization, as a type of business, had only just been established and were aimed at search results for Yahoo and Rambler.</p> 

<p>We can proudly say that, for 20 years, we have used and continue to use only "white hat" (ethical) methods for promoting sites on Yandex and Google.</p> 

<p>To assist with SEO, Wolf.ru specialists send targeted search engine users, who are specifically looking for your goods or services and are ready to buy, to your site.</p> 

<p>Wolf.ru does not use automated search optimization. We promote all sites using the most professional methods - manually - in order to achieve reliable results. Internal competition between clients is not a problem because we never take on more than three sites dealing with the same subject matter.</p>
',
    'adv1h' => '"White hat methods"',
    'adv1t' => 'We only use only ethical tactics for website promotion.',
    'adv2h' => 'Unique development',
    'adv2t' => 'Wolf has its own software geared toward quick traffic collection.',
    'adv3h' => 'Invaluable experience',
    'adv3t' => 'Wolf has worked in this market for 20 years. We started before Yandex even existed.',
    'adv4h' => 'Client recommendations',
    'adv4t' => 'More than 80 percent of new clients come to us through recommendations.',
    'adv5h' => 'Targeted traffic',
    'adv5t' => 'All methods of promotion are aimed at targeted and relevant traffic.',
    'adv6h' => 'Detailed reporting',
    'adv6t' => 'All of the company\'s work is supported by monthly detailed reports.',
    'serh' => 'We offer Wolf.ru clients two options for collaboration:',
    'sert1' => 'Promotion by ranking',
    'sert2' => 'Promotion by traffic',
    'serit1h' => '01 | Promotion by ranking',
    'serit1t' => '<p>The more frequently a particular word appears on search engines, the more time it will take to rank higher. It\'s possible to promote your website through high-frequency, medium- and low-frequency queries. Choosing which keywords to include depends not only on the goals you have for your site but also on competition within your field and on the size of the budget you plan to use to promote your site.</p>
<p>Our specialists will prepare a promotion strategy for your site that will best take into account the features you want, as well as your limits. The technological structure we use will allow your site to achieve the desired results in the shortest time possible, regardless of any disaster situations that may arise. We don\'t hide the technology behind our work and and offer a look at the steps it will take to promote your website, which include:</p>',
    'serst1' => 'Step <span>1</span>',
    'serst1h' => 'Establishing technical specifications',
    'serst1t' => 'The first step includes preparatory work: developing technical specifications for the programmers with the goal of adapting the technical side of the website for further optimization and promotion.',
    'serst2' => 'Step <span>2</span>',
    'serst2h' => 'Link-building strategy',
    'serst2t' => 'The second step includes developing link-building strategies, accumulating primary links, revising text content, refining optimization and analyzing results. ',
    'serst3' => 'Step <span>3</span>',
    'serst3h' => 'Optimizing text, strategy correction',
    'serst3t' => 'The third step includes continuing to fill the site with optimized text, intensive systematic link-building, refining site optimization, analyzing results and correcting strategies as search engine algorithms change. ',
    'serit2h' => '02 | Promotion by traffic ',
    'serit2t' => '<p>Maybe you\'ve heard of traffic promotion for websites, but you don\'t fully realize its potential. </p>
<p>The obvious advantage in traffic promotion for your site is that it allows a search for all the specific goods and services that interest your audience.</p>
<p>Keywords and search engines are like a lock and key that have to match; if they don\'t fit together, the site\'s traffic promotion won\'t work. </p>
<p>We choose only combinations of keywords that are relevant to your site\'s content and search engines that are highly efficient. We use hundreds of combinations that will get your site to the TOP of search engines, as well as increase visitors to your site. </p>',
    'serbottom' => 'Every website is unique; the promotion strategy for each site is individualized and depends on many factors: quality of the website/domain age/code cleanliness/content component/ease of use (usability). ',
    'mainFormHeader' => 'Learn the value of promoting your website',
    'mainFormText' => 'In order for us to prepare a free audit and offer a business proposal, simply enter your website address and leave your contact information on our site.',
    'stepsHeader' => 'Simple steps toward achieving results',
    'step1' => 'We create an individualized strategy of search promotion for the project.',
    'step2' => 'We establish the technical specifications for the internal optimization of the site.',
    'step3' => 'We customize the site and webmaster tools for search engines.',
    'step4' => 'We improve behavioral factors and increase the feed capacity and site conversion.',
    'step5' => 'We increase the relevance of landing pages and the site with the goal of implementing content development.',
    'step6' => 'On a weekly basis, we control critical indicators of optimization and site efficiency.',
    'Site address' => '',
    'Name' => '',
    'Email' => '',
    'Phone' => '',
    'requireFields' => '* Fields, requires for filling',
    'takeRequest' => 'Leave a request',
    'Order calculation' => '',
    'Agency presentation' => '',
    '' => '',
    '' => '',
    '' => '',
];

?>