<?php

namespace common\models;

use Yii;
use \common\models\base\Textpage as BaseTextpage;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "textpage".
 */
class Textpage extends BaseTextpage
{

public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                [
                    'class' => SluggableBehavior::className(),
                    'attribute' => 'name',
                    'slugAttribute' => 'alias',
                    'immutable' => true
                ],
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
             parent::rules(),
             [
                  # custom validation rules
             ]
        );
    }
    
    public static function liLink($id, $class) {
        $model = Textpage::findOne($id);

        if ($_SERVER['REQUEST_URI'] == '/'.$model->alias) {
            $active = ' class="active"';
        } else {
            $active = '';
        }

        $name = '';
        if (Yii::$app->language == 'en') {
            $name = $model->en_name;
        } else {
            $name = $model->name;
        }

        return '<li'.$active.'><a href="/'.$model->alias.'" class="'.$class.'"><span>'.$name.'</span></a></li>';
    }
}
