<?php

namespace common\models;

use himiklab\sortablegrid\SortableGridBehavior;
use Yii;
use \common\models\base\Client as BaseClient;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "client".
 */
class Client extends BaseClient
{

public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                'sort' => [
                    'class' => SortableGridBehavior::className()
                ],
            ]
        );
    }

    public function rules()
    {
        return [
            [['sortOrder'], 'integer'],
            [['name', 'image', 'introtext', 'en_introtext'], 'required'],
            [['text', 'en_text'], 'string'],
            [['queries', 'en_queries'], 'safe'],
            [['name', 'link', 'en_link', 'image', 'introtext', 'en_introtext'], 'string', 'max' => 255]
        ];
    }
}
