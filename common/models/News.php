<?php

namespace common\models;

use Yii;
use \common\models\base\News as BaseNews;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "news".
 */
class News extends BaseNews
{

public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                [
                    'class' => SluggableBehavior::className(),
                    'attribute' => 'name',
                    'slugAttribute' => 'alias',
                    'immutable' => true
                ],
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
             parent::rules(),
             [
                 [['date'], 'safe'],
             ]
        );
    }

    public static function pagination($allItemsCount, $page, $limit) {
        if ($limit >= $allItemsCount) {
            return '';
        } else {
            $pages = [];
            $_GET['page'] = (isset($_GET['page']))? $_GET['page'] : 1;
            $lastPage = ceil($allItemsCount / $limit);
            $uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
            $prev = (($_GET['page'] - 1) == 1)? $uri_parts[0] : $uri_parts[0] . '?page=' . ($_GET['page'] - 1);

            if ($page != 1) {
                $pages[] = '<li><a href="' . $uri_parts[0] . '" class="pagination__item pagination__first">&lt;&lt;</a></li>';
                $pages[] = '<li><a href="' . $prev . '" class="pagination__item pagination__first">&lt;</a></li>';
            }

            for($i = 1; $i <= $lastPage; $i++) {
                $link = ($i == 1)? $uri_parts[0] : $uri_parts[0] . '?page=' . $i;
                if ($i == $page) {
                    $pages[] = '<li class="active"><span class="pagination__item">' . $i . '</span></li>';
                } else {
                    $pages[] = '<li><a href="' . $link . '" class="pagination__item">' . $i . '</a></li>';
                }
            }

            if ($page != $lastPage) {
                $pages[] = '<li><a href="' . $uri_parts[0] . '?page=' . ($page + 1) . '" class="pagination__item pagination__last">&gt;</a></li>';
                $pages[] = '<li><a href="' . $uri_parts[0] . '?page=' . ($lastPage) . '" class="pagination__item pagination__last">&gt;&gt;</a></li>';
            }

            return '<ul class="pagination">'.implode('', $pages).'</ul>';
        }
    }
}
