<?php
// This class was automatically generated by a giiant build task
// You should not change it manually as it will be overwritten on next build

namespace common\models\base;

use Yii;

/**
 * This is the base-model class for table "client".
 *
 * @property integer $id
 * @property string $sortOrder
 * @property string $name
 * @property string $link
 * @property string $en_link
 * @property string $image
 * @property string $introtext
 * @property string $en_introtext
 * @property string $text
 * @property string $en_text
 * @property string $queries
 * @property string $en_queries
 * @property string $aliasModel
 */
abstract class Client extends \yii\db\ActiveRecord
{



    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sortOrder'], 'integer'],
            [['name', 'image', 'introtext', 'en_introtext', 'text', 'en_text'], 'required'],
            [['text', 'en_text', 'queries', 'en_queries'], 'string'],
            [['name', 'link', 'en_link', 'image', 'introtext', 'en_introtext'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sortOrder' => 'Сортировка',
            'name' => 'Название',
            'link' => 'Ссылка(если не заполнено, ссылка будет из названия)',
            'en_link' => 'Link',
            'image' => 'Изображение(329x329)',
            'introtext' => 'Введение',
            'en_introtext' => 'Introtext',
            'text' => 'Текст',
            'en_text' => 'Text',
            'queries' => 'Запросы',
            'en_queries' => 'Queries',
        ];
    }




}
