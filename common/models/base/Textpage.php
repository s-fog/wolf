<?php
// This class was automatically generated by a giiant build task
// You should not change it manually as it will be overwritten on next build

namespace common\models\base;

use Yii;

/**
 * This is the base-model class for table "textpage".
 *
 * @property integer $id
 * @property string $name
 * @property string $en_name
 * @property string $alias
 * @property string $seo_title
 * @property string $en_seo_title
 * @property string $seo_keywords
 * @property string $en_seo_keywords
 * @property string $seo_h1
 * @property string $en_seo_h1
 * @property string $seo_description
 * @property string $en_seo_description
 * @property string $aliasModel
 */
abstract class Textpage extends \yii\db\ActiveRecord
{



    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'textpage';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'en_name'], 'required'],
            [['seo_description', 'en_seo_description'], 'string'],
            [['name', 'en_name', 'alias', 'seo_title', 'en_seo_title', 'seo_keywords', 'en_seo_keywords', 'seo_h1', 'en_seo_h1'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'en_name' => 'En Name',
            'alias' => 'Урл',
            'seo_title' => 'Seo Title',
            'en_seo_title' => 'En Seo Title',
            'seo_keywords' => 'Seo Keywords',
            'en_seo_keywords' => 'En Seo Keywords',
            'seo_h1' => 'Seo H1',
            'en_seo_h1' => 'En Seo H1',
            'seo_description' => 'Seo Description',
            'en_seo_description' => 'En Seo Description',
        ];
    }




}
