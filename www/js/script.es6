class Cities {
    constructor(root) {
        this.root = root;

        this._cacheNodes();
        this._bindEvents();
        this._ready();
    }

    _cacheNodes() {
        this.nodes = {

        }
    }

    _bindEvents() {

    }

    _ready() {

    }
}

class Application {
    constructor() {
        this._mainScripts();
        this._initClasses();
    }

    _mainScripts() {
        $('.mainSlider__inner').owlCarousel({
            items: 1,
            nav: true,
            navText: false,
            dots: false,
            loop: true,
            autoplay: true,
            autoplayTimeout: 7000,
            autoplayHoverPause: true
        });

        let yandex__inner = $('.yandex__inner');
        yandex__inner.on('initialized.owl.carousel', function() {
            $('.yandex').css('height', 'auto');
        });
        yandex__inner.owlCarousel({
            items: 1,
            nav: true,
            navText: false,
            dots: false,
            loop: true
        });

        if($('#map').get(0)){
            google.maps.event.addDomListener(window, 'load', init);
        }

        flexGridAddElements('company__faces', 'company__face', 'company__face_hidden');
        flexGridAddElements('clients__inner', 'clients__item', 'clients__item_hidden');


        function init() {
            // Basic options for a simple Google Map
            // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
            var mapOptions = {
                // How zoomed in you want the map to start at (always required)
                zoom: 11,
                disableDefaultUI: true,
                // The latitude and longitude to center the map (always required)
                center: new google.maps.LatLng(55.7344825, 37.5891488),

                // How you would like to style the map.
                // This is where you would paste any style found on Snazzy Maps.
                styles: [
                    {"featureType":"all","elementType":"labels.text.fill","stylers":[
                        {"saturation":36},{"color":"#000000"},{"lightness":40}
                    ]},
                    {"featureType":"all","elementType":"labels.text.stroke","stylers":[
                        {"visibility":"on"},{"color":"#000000"},{"lightness":16}]
                    },
                    {"featureType":"all","elementType":"labels.icon","stylers":[
                        {"visibility":"off"}
                    ]},
                    {"featureType":"administrative","elementType":"geometry.fill","stylers":[
                        {"color":"#000000"},{"lightness":20}
                    ]},
                    {"featureType":"administrative","elementType":"geometry.stroke","stylers":[
                        {"color":"#000000"},{"lightness":17},{"weight":1.2}
                    ]},
                    {"featureType":"landscape","elementType":"geometry","stylers":[
                        {"color":"#000000"},{"lightness":20}
                    ]},
                    {"featureType":"poi","elementType":"geometry","stylers":[
                        {"color":"#000000"},{"lightness":21}
                    ]},
                    {"featureType":"road.highway","elementType":"geometry.fill","stylers":[
                        {"color":"#000000"},{"lightness":17}]
                    },
                    {"featureType":"road.highway","elementType":"geometry.stroke","stylers":[
                        {"color":"#000000"},{"lightness":29},{"weight":0.2}
                    ]},
                    {"featureType":"road.arterial","elementType":"geometry","stylers":[
                        {"color":"#000000"},{"lightness":18}]
                    },
                    {"featureType":"road.local","elementType":"geometry","stylers":[
                        {"color":"#000000"},{"lightness":16}
                    ]},
                    {"featureType":"transit","elementType":"geometry","stylers":[
                        {"color":"#000000"},{"lightness":19}
                    ]},
                    {"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":17}]}]
            };

            // Get the HTML DOM element that will contain your map
            // We are using a div with id="map" seen below in the <body>
            var mapElement = document.getElementById('map');

            // Create the Google Map using our element and options defined above
            var map = new google.maps.Map(mapElement, mapOptions);
            var image = '/img/baloon.png';
            // Let's also add a marker while we're at it
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(55.7344825, 37.5891488),
                map: map,
                title: 'Roiter',
                icon: image
            });
        };

        $(window).resize(() => {
            this._mapContent();
        });

        $('.usluga__tab').on('click', function() {
            let index = $(this).index();

            $("html, body").animate({ scrollTop: $('.usluga__content').eq(index).offset().top - 20 }, 300);
        });



        this._mapContent();
    }

    _initClasses() {
        new Cities();
    }

    _mapContent() {
        let windowWidth = parseInt($(window).width());
        let containerWidth = parseInt($('.container').width());
        let left;

        if (windowWidth > containerWidth) {
            left = (windowWidth - containerWidth) / 2;
        } else {
            left = 0;
        }

        $('.map__content').css('left', left + 'px');

        $('.sendForm').submit((event) => {
            event.preventDefault();
            var form = $(event.currentTarget);
            var fields = form.find('input, textarea, select');
            var validate = true;

            fields.each((index, element) => {
                if ($(element).hasClass('required')) {
                    if (element.value == '') {
                        $(element).addClass('error');
                        validate = false;
                    } else {
                        $(element).addClass('validateSuccess');
                        $(element).removeClass('error');
                    }
                } else {
                    $(element).addClass('validateSuccess');
                }
            });

            if (!validate) {
                return false;
            }

            var formData = new FormData(form.get(0));
            var xhr = new XMLHttpRequest();
            xhr.open("POST", "/mail/index");
            xhr.send(formData);

            xhr.upload.onprogress = () => {

            };

            xhr.onreadystatechange = () => {
                if (xhr.readyState == 4){
                    if (xhr.status == 200){
                        var response = xhr.responseText;

                        if (response == 'success') {
                            $.fancybox.close();

                            setTimeout(() => {
                                $.fancybox.open('<div class="success__wrapper">'+$('#success').html()+'</div>');
                            }, 200);

                            setTimeout(() => {
                                $.fancybox.close();
                            }, 5000);
                        } else {
                            alert('Ошибка');
                        }
                    } else {
                        console.log('error status');
                    }
                }
            };
        });
    }
}

(function () {
    new Application();
})();